#ifndef _SUBJECT_HPP_
#define _SUBJECT_HPP_

#include <string>
#include "Student.h"
#include "Database.h"

class Student;

class Subject
{
public:
	Subject(std::string name, std::string professor_name);
	const std::string & GetName() const;
	void examination(Database* db, Student* stud, unsigned int score, int id);
	std::string GetProfessorName() const;
private:
	Subject(const Subject &);
	Subject & operator = (const Subject &);

private:
	const std::string m_name;
	std::string m_professor_name;
};

inline const std::string & Subject::GetName() const
{
	return m_name;
}

inline std::string Subject::GetProfessorName() const
{
	return m_professor_name;
}

#endif
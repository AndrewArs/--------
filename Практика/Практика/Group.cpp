#define _CRT_SECURE_NO_WARNINGS
#include "Group.h"

Group::Group(const std::string & name)
	:m_name(name)
{

}

void Group::Add(Student* stud)
{
	m_students.push_back(stud);
}

int Group::AppointSteward(Student* stud)
{
	for (int i = 0; i < m_students.size(); i++)
	{
		if (m_students.at(1) == stud)
		{
			m_steward = stud;
			return 1;
		}
	}

	return 0;
}

int Group::Exclusion(Student* stud, Group* group, Database* db)
{
	char buffer[100];
	for (int i = 0; i < m_students.size(); i++)
	{
		if (m_students.at(i) == stud)
		{
			m_students.erase(m_students.begin() + i);
			group->Add(stud);
			sprintf(buffer, "UPDATE students SET group_name = \"%s\" WHERE name = \"%s\"",
				group->GetName().c_str(), stud->GetName().c_str());
			db->SQLExec(buffer);
			return 1;
		}
	}

	return 0;
}

const Student* Group::GetStudentAt(int pos)
{
	return m_students.at(pos);
}
#include <iostream>
#include "sqlite3.h"
#include "Group.h"
#include "Student.h"
#include "Subject.h"
#include "Database.h"

int main()
{
	std::vector<Student*> stud;
	Database db("DB.db");
	Group group1("KI-13-5");
	Group group2("KI-13-4");
	Student stud1("Kirill Galanov", &group1, 0);
	Student stud2("Pet'a Petichkin", &group2, 0);
	stud1.put(&db);
	stud2.put(&db);
	db.CoutTable("students");

	//Student stud3("Vas'a Pirozhkov", &group1, 1);
	//stud3.put(&db);
	//db.CoutTable("students");

	Subject sub1("Math", "Math Professor");
	stud1.AddSubject(&sub1);
	sub1.examination(&db, &stud1, 90, 0);
	db.CoutTable("examination_results");

	stud = stud1.all(&db);
	/*group.Exclusion(&stud1, &group1, &db);
	db.CoutTable("students");
	Student stud3("Vas'a Pirozhkov", &group, 1);
	stud3.put(&db);
	db.CoutTable("students");*/

	system("pause");
	return 0;
}

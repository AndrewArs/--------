#define _CRT_SECURE_NO_WARNINGS
#include "Database.h"

Database::Database(std::string db_name)
	:m_db_name(db_name)
{
	
	int rc;
	char *errmsg;
	rc = sqlite3_open(db_name.c_str(), &m_db);

	if (rc) {
		printf("Error: %s\n", sqlite3_errmsg(m_db));
		sqlite3_close(m_db);
	}
}

static int callback(void *notused, int coln, char **rows, char **colnm)
{
	/*
	coln - ����� �������� � ������������� �������
	rows - ������ �������� ������
	colnm - ����� ��������
	*/
	int i;
	static int b = 1;
	// ������ �������� ��������
	if (b) // �������� ������ ���� ���
	{
		for (i = 0; i<coln; i++)
			printf("%s\t\t", colnm[i]);
		printf("\n");
		b = 0;
	}
	// ����������� �����
	for (i = 0; i<coln; i++)
		printf("--------------\t");
	printf("\n");
	//������ ��������
	for (i = 0; i<coln; i++)
		printf("%s\t|", rows[i]);
	printf("\n");
	return 0;
} // end callback

void Database::CoutTable(std::string table) const
{
	char buffer[50];
	sprintf(buffer, "select * from %s;", table.c_str());
	sqlite3_exec(m_db, buffer, callback, NULL, NULL);
}

int Database::SQLExec(const char *sql)
{
	int rc;

	if (sqlite3_open(m_db_name.c_str(), &m_db))
		return 0;

	if (sqlite3_prepare(m_db, sql, -1, &m_pStmt, NULL))
	{
		printf("Error: %s\n", sqlite3_errmsg(m_db));
		sqlite3_finalize(m_pStmt);
		sqlite3_close(m_db);
		return 0;
	}

	rc = sqlite3_step(m_pStmt);

	if (rc != SQLITE_DONE) 	
		printf("Error: %s\n", sqlite3_errmsg(m_db));

	sqlite3_finalize(m_pStmt);

	return 1;
}

int Database::Count(std::string table)
{
	char buffer[50];
	sprintf(buffer, "SELECT COUNT(Stud_id) FROM %s;", table.c_str());
	sqlite3_prepare(m_db, buffer, -1, &m_pStmt, NULL);
	sqlite3_step(m_pStmt);
	m_counter = sqlite3_column_int(m_pStmt, 0);
	sqlite3_finalize(m_pStmt);
	return	m_counter;
}

char* Database::GetChar(const char *sql) 
{
	const unsigned char* result;

	if (sqlite3_prepare(m_db, sql, -1, &m_pStmt, NULL))
	{
		printf("Error: %s\n", sqlite3_errmsg(m_db));
		sqlite3_finalize(m_pStmt);
		sqlite3_close(m_db);
		return 0;
	}

	sqlite3_step(m_pStmt);

	result = sqlite3_column_text(m_pStmt, 0);

	//sqlite3_finalize(m_pStmt);

	return (char*)result;
}
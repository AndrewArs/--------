#ifndef _GROUP_HPP_
#define _GROUP_HPP_

#include <string>
#include <vector>
#include "Database.h"
#include "Student.h"

class Student;

class Group
{
public:
	Group(const std::string & name);
	const Student* GetStudentAt(int pos);
	void Add(Student* stud);
	int Exclusion(Student* stud, Group* group, Database* db);
	int AppointSteward(Student* stud);
	const std::string & GetName() const;
	Student* GetSteward() const;

private:
	Group(const Group &);
	Group & operator = (const Group &);

private:
	const std::string m_name;
	Student* m_steward;
	std::vector <const Student* > m_students;
};

inline const std::string & Group::GetName() const
{
	return m_name;
}

inline Student* Group::GetSteward() const
{
	return m_steward;
}

#endif;
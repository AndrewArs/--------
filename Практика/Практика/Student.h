#ifndef _STUDENT_HPP_
#define _STUDENT_HPP_

#include <string>
#include <vector>
#include "Group.h"
#include "Database.h"
#include "Subject.h"

class Group;
class Subject;

class Student
{
public:
	Student(const std::string & name, Group* group, int id);
	void AddSubject(Subject* sub);
	bool SearchSubject(Subject* sub);
	void put(Database* db);
	void Delete(Database* db);
	std::vector<Student* > all(Database* db);
	const std::string & GetName() const;
	const int & GetID() const;

private:
	Student(const Student &);
	Student & operator = (const Student &);

private:
	const std::string m_name;
	std::string m_group;
	int m_id = 0;
	std::vector <const Subject* > m_subjects;
};

inline const std::string & Student::GetName() const
{
	return m_name;
}

inline const int & Student::GetID() const
{
	return m_id;
}
#endif
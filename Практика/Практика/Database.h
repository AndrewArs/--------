#ifndef _DATABASE_HPP_
#define _DATABASE_HPP_

#include <iostream>
#include <string>
#include "sqlite3.h"

class Database
{
public:
	// ����������� ��������� ��
	Database(std::string db_name);

	//���������� ��������� �� 
	~Database() { sqlite3_close(m_db); }

	void CoutTable(std::string table) const;

	char* GetChar(const char *sql);

	int Count(std::string table);

	// ���������� SQL �������
	int SQLExec(const char *sql);

private:
	int m_counter;
	std::string m_db_name;
	sqlite3 *m_db;
	sqlite3_stmt *m_pStmt;
}; 

#endif
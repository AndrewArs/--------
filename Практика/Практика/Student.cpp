#define _CRT_SECURE_NO_WARNINGS
#include "Student.h"

Student::Student(const std::string & name, Group* group, int id)
	:m_name(name),
	m_group(group->GetName().c_str()),
	m_id(id)
{
	group->Add(this);
}

void Student::put(Database* db)
{
	char buffer[150];
	if (m_id == 0)
	{
		m_id = db->Count("students") + 1;
		sprintf(buffer, "INSERT INTO students VALUES(%d, \"", m_id);
		strcat(buffer, m_name.c_str());
		strcat(buffer, "\", \"");
		strcat(buffer, m_group.c_str());
		strcat(buffer, "\");");
	}
	else
	{
		sprintf(buffer,
			"UPDATE students SET name = \"%s\" WHERE stud_id = %d;",
			m_name.c_str(), m_id);
		db->SQLExec(buffer);
		sprintf(buffer,
			"UPDATE students SET group_name = \"%s\" WHERE stud_id = %d;",
			m_group.c_str(), m_id);
	}
	//printf(buffer);
	db->SQLExec(buffer);
}

void Student::Delete(Database* db)
{
	char buffer[100];
	sprintf(buffer, "DELETE FROM students WHERE stud_id = %d; VACUUM;", m_id);
}

std::vector<Student* > Student::all(Database* db)
{
	std::vector<Student*> stud;
	char buffer1[100];
	char buffer2[100];

	for (int i = 1; i <= db->Count("students"); i++)
	{
		sprintf(buffer1, "SELECT name FROM students WHERE stud_id = %d;", i);
		sprintf(buffer2, "SELECT group_name FROM students WHERE stud_id = %d;", i);
		std::string str(db->GetChar(buffer2));
		stud.push_back(new Student(db->GetChar(buffer1), &Group(str), i));
	}

	return stud;
}

void Student::AddSubject(Subject* sub)
{
	m_subjects.push_back(sub);
}

bool Student::SearchSubject(Subject* sub)
{
	for (int i = 0; i < m_subjects.size(); i++)
	{
		if (m_subjects.at(i)->GetName() == sub->GetName())
			return true;
	}

	return false;
}
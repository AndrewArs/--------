#define _CRT_SECURE_NO_WARNINGS
#include "Subject.h"

Subject::Subject(std::string name, std::string professor_name)
	:m_name(name), m_professor_name(professor_name)
{

}

void Subject::examination(Database* db, Student* stud, unsigned int score, int id)
{
	char buffer[100];

	if (stud->GetID() == 0)
	{
		printf("\nThe student must be in the database!");
		return;
	}

	if (score > 100)
	{
		printf("\nScore must be less than 100!");
		return;
	}

	if (stud->SearchSubject(this) == false)
	{
		printf("\nThis student is not studying this subject!");
		return;
	}

	if (id == 0)
	{
		sprintf(buffer, "INSERT INTO examination_results VALUES(%d, %d, \"%s\", %d)",
			db->Count("examination_results") + 1, stud->GetID(), m_name.c_str(), score);
	}
	else
	{
		sprintf(buffer,
			"UPDATE examination_results SET stud_id = %d, subject = \"%s\", score = %d WHERE id = %d;",
			stud->GetID(), this->GetName(), score, id);
		printf("\n%s", buffer);
	}

	db->SQLExec(buffer);
}